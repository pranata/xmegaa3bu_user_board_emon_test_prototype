#ifndef _OWI_OWIAPP_H
#define _OWI_OWIAPP_H

#include "user_board.h"

// Defines used only in code example.
#define DS1820_FAMILY_ID                0x28
#define DS1820_START_CONVERSION         0x44
#define DS1820_READ_SCRATCHPAD          0xbe
#define DS1820_ERROR                    -1000   // Return code. Outside temperature range.

#define DS2890_FAMILY_ID                0x2c
#define DS2890_WRITE_CONTROL_REGISTER   0X55
#define DS2890_RELEASE_CODE             0x96
#define DS2890_WRITE_POSITION           0x0f

#define SEARCH_SUCCESSFUL               0x00
#define SEARCH_CRC_ERROR                0x01

#define FALSE       0
#define TRUE        1

#define MAX_DEVICES 8       //!< Max number of devices to search for.

#define BUSES   (OWI_BUS_1) //!< Buses to search.


/*! \brief  Data type used to hold information about slave devices.
 *  
 *  The OWI_device data type holds information about what bus each device
 *  is connected to, and its 64 bit identifier.
 */
typedef struct
{
    unsigned char bus;      //!< A bitmask of the bus the device is connected to.
    unsigned char id[8];    //!< The 64 bit identifier.
} OWI_device;

// Prototypes of functions used in exemples.
unsigned char SearchBuses(OWI_device * devices, unsigned char len, unsigned char buses);
OWI_device * FindFamily(unsigned char familyID, OWI_device * devices, unsigned char size);
signed int DS1820_ReadTemperature(unsigned char bus, unsigned char * id);
void DS2890_SetWiperPosition(unsigned char position, unsigned char bus, unsigned char * id);


#endif