/**
 * \file
 *
 * \brief User board initialization template
 *
 */

#include <board.h>
#include <conf_board.h>
#include <ioport.h>

void board_init(void)
{
	/* This function is meant to contain board-specific initialization code
	 * for, e.g., the I/O pins. The initialization can rely on application-
	 * specific board configuration, found in conf_board.h.
	 */
	 
	ioport_configure_pin(LED1_GPIO, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);
	ioport_configure_pin(LED2_GPIO, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);

	ioport_configure_pin(RELAY1_GPIO, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);
	ioport_configure_pin(RELAY2_GPIO, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);
	
	ioport_configure_pin(MAINS_VOLTAGE_SIGNAL_PIN, IOPORT_DIR_INPUT | IOPORT_INIT_LOW | IOPORT_TOTEM | IOPORT_INPUT_DISABLE);
	
	ioport_configure_pin(USARTC0_TXD_GPIO, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);
	ioport_configure_pin(USARTC0_RXD_GPIO, IOPORT_DIR_INPUT);
}
	
