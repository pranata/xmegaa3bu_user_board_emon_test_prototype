/**
 * \file
 *
 * \brief User board definition template
 * To use this board define BOARD=USER_BOARD.
 */

#ifndef USER_BOARD_H
#define USER_BOARD_H

/* This file is intended to contain definitions and configuration details for
 * features and devices that are available on the board, e.g., frequency and
 * startup time for an external crystal, external memory devices, LED and USART
 * pins.
 */

#include <compiler.h>
#include "OWIPolled.h"

/**
 * \name LEDs
 *
 * LED1 and LED2 are single yellow LEDs that are active low.
 */
//@{
#define LED1_GPIO       IOPORT_CREATE_PIN(PORTE, 0)
#define LED2_GPIO       IOPORT_CREATE_PIN(PORTE, 1)
#define LED1            LED1_GPIO
#define LED2            LED2_GPIO
//! Number of LEDs
#define LED_COUNT       2
//@}


/**
 * \name Temperature sensors
 *
 * Temperature sensors are DS18B20's, connected to GPIO pin
 */
//@{
#define TEMP_K1_GPIO    IOPORT_CREATE_PIN(PORTB, 1)
#define TEMP_K2_GPIO    IOPORT_CREATE_PIN(PORTB, 2)
#define TEMP_K3_GPIO    IOPORT_CREATE_PIN(PORTB, 3)
#define TEMP_K1         TEMP_K1_GPIO
#define TEMP_K2         TEMP_K2_GPIO
#define TEMP_K3         TEMP_K3_GPIO
#define OWI_BUS_1       OWI_PIN_1
#define OWI_BUS_2       OWI_PIN_2
#define OWI_BUS_3       OWI_PIN_3
//@}


/**
 * \name Relays
 *
 * Relays are connected through optocouplers.
 * Active-low: drive the IO port low to activate relay.
 */
//@{
#define RELAY1_GPIO   IOPORT_CREATE_PIN(PORTA, 5)
#define RELAY2_GPIO   IOPORT_CREATE_PIN(PORTA, 6)
#define RELAY1        RELAY1_GPIO
#define RELAY2        RELAY2_GPIO
//@}
 
 
/**
 * \name External oscillator
 *
 * \todo Need to measure the actual startup time on the hardware.
 */
//@{
#define BOARD_XOSC_HZ           16000000
#define BOARD_XOSC_TYPE         XOSC_TYPE_XTAL
#define BOARD_XOSC_STARTUP_US   1024
//@}


/** \name Mains voltage measurement
 *
 *  \brief The voltage measurement is done through a small mains transformer.
 *         The secondary winding is connected to ADC pin through a filter/rectifier circuit.
 */
//@{
#define MAINS_VOLTAGE_ADC_MODULE   ADCB
#define MAINS_VOLTAGE_ADC_INPUT    ADCCH_POS_PIN0
#define MAINS_VOLTAGE_SIGNAL_PIN   IOPORT_CREATE_PIN(PORTB, 0)
//@}


/** \name UART pins
 */
//@{
#define USARTC0_RXD_GPIO           IOPORT_CREATE_PIN(PORTC, 2)
#define USARTC0_TXD_GPIO           IOPORT_CREATE_PIN(PORTC, 3)
//@}


/**
 * \name Pin connections on header P4
 */
//@{
#define P4_PIN0                        IOPORT_CREATE_PIN(PORTC, 0)
#define P4_PIN1                        IOPORT_CREATE_PIN(PORTC, 1)
#define P4_PIN2                        IOPORT_CREATE_PIN(PORTC, 2)
#define P4_PIN3                        IOPORT_CREATE_PIN(PORTC, 3)
#define P4_PIN4                        IOPORT_CREATE_PIN(PORTC, 4)
#define P4_PIN5                        IOPORT_CREATE_PIN(PORTC, 5)
#define P4_PIN6                        IOPORT_CREATE_PIN(PORTC, 6)
#define P4_PIN7                        IOPORT_CREATE_PIN(PORTC, 7)
//@}



#endif // USER_BOARD_H
