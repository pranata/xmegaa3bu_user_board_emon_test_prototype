/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * AVR Software Framework (ASF).
 */
#define F_CPU sysclk_get_cpu_hz()

#include <asf.h>
#include <board.h>
#include <util/delay.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <adc.h>

#include "OWIPolled.h"
#include "OWIBitFunctions.h"
#include "OWIapp.h"

volatile uint32_t measurement_data;
volatile uint8_t measurement_data_ready = 0;
volatile int16_t measurement_samples[128];
volatile uint8_t sample_count = 0;

/** \brief Accumulate 128 samples from ADCB, which is triggered by TCC0 overflow */
void adc_handler(ADC_t *adc, uint8_t ch_mask, adc_result_t result)
{
	// Only process result coming from ADCB ch #0
	if ((adc != &MAINS_VOLTAGE_ADC_MODULE) || (ch_mask != ADC_CH0)) return;
	
	gpio_toggle_pin(LED2_GPIO);
	
	if (sample_count == 0) {
		measurement_data_ready = 0;
	}
	
	measurement_data = measurement_data + ((uint32_t) (result * result));
	measurement_samples[sample_count] = result;
	sample_count++;
	
	if (sample_count >= 128) {
		tc_disable(&TCC0);
		sample_count = 0;
		measurement_data_ready = 1;	
	}
}	

/**
 * \brief Timer Counter initialization
 */
void timer_init(void)
{
	/* For the event system, it must be selected what event is to be routed through the multiplexer for each
	* event channel used */
  
	/* Select TCC0 overflow as event channel 0 multiplexer input.
	* This is all code required to configure one event channel */
	EVSYS.CH0MUX = EVSYS_CHMUX_TCC0_OVF_gc;
  
	/*
	* Unmask clock for TIMER_EXAMPLE
	*/
	tc_enable(&TCC0);
	
	/*
	* Configure TC in normal mode, configure period, CCA and CCB
	* Enable both CCA and CCB channels
	* Overflow period = 16MHz / 5000 = 3.2kHz = 64 samples * 50 Hz
	*/
	tc_set_wgm(&TCC0, TC_WG_NORMAL);
	tc_write_period(&TCC0, 5000-1);

	/* Set clock divider to /1
	   16.0 MHz */
	tc_write_clock_source(&TCC0, TC_CLKSEL_DIV1_gc);
	
	tc_disable(&TCC0);
}

/**
 * \brief Initialize ADC channels for NTC and lightsensor
 *
 * This will set up the ADC for reading the NTC and light sensor
 * present on the A3BU-Xplained board.
 */
void adc_sensors_init(void)
{
	struct adc_config adc_conf;
	struct adc_channel_config adc_ch_conf;

	/* Clear the ADC configuration structs */
	adc_read_configuration(&MAINS_VOLTAGE_ADC_MODULE, &adc_conf);
	adcch_read_configuration(&MAINS_VOLTAGE_ADC_MODULE, ADC_CH0, &adc_ch_conf);

	/* configure tha ADCA module:
	- signed, 12-bit resolution
	- VCC/2 reference
	- 2MHz max clock rate
	- manual conversion triggering
	*/
	adc_set_conversion_parameters(&adc_conf, ADC_SIGN_ON, ADC_RES_12,
			ADC_REF_BANDGAP);
	adc_set_clock_rate(&adc_conf, 31250UL);
	adc_set_conversion_trigger(&adc_conf, ADC_TRIG_EVENT_SINGLE, 1, 0);
	adc_set_gain_impedance_mode(&adc_conf, ADC_GAIN_LOWIMPEDANCE);
	adc_write_configuration(&MAINS_VOLTAGE_ADC_MODULE, &adc_conf);
	adc_set_callback(&MAINS_VOLTAGE_ADC_MODULE, &adc_handler);

	/* Configure ADC A channel 0:
	 * - single-ended measurement
	 * - interrupt flag set on completed conversion
	 */
	adcch_set_input(&adc_ch_conf, ADCCH_POS_PIN15, ADCCH_NEG_PIN0, 1);
	adcch_set_interrupt_mode(&adc_ch_conf, ADCCH_MODE_COMPLETE);
	adcch_enable_interrupt(&adc_ch_conf);
	adcch_write_configuration(&MAINS_VOLTAGE_ADC_MODULE, ADC_CH0, &adc_ch_conf);

	adc_enable(&MAINS_VOLTAGE_ADC_MODULE);
}

int main (void)
{
	char line_buffer[256];
	char* p_line_buffer = line_buffer;
	uint8_t line_buffer_full = 0;

	board_init();

	
	irq_initialize_vectors();
	cpu_irq_enable();
	
	sleepmgr_init();
	sysclk_init();
	sysclk_enable_peripheral_clock(&EVSYS);
// 	gpio_set_pin_low(LED1_GPIO);
// 	while (1);
	
	/* ADC will be triggered by timer overflow.
	   Timer is disabled at initialization.
	   To start conversion, enable the timer (see parser below)
	*/
	adc_sensors_init();
	timer_init();
	
	
	udc_start();
	udc_attach();
	
	while (1) {
		if (udi_cdc_is_rx_ready() && (line_buffer_full == 0)) {
			*p_line_buffer = udi_cdc_getc();
			udi_cdc_putc(*p_line_buffer);
			
			// Check if ENTER/RETURN is received. Change to \0 as string termination.
			if (*p_line_buffer == '\n' || *p_line_buffer == '\r') {
				*p_line_buffer = '\0';
				p_line_buffer = line_buffer;
				line_buffer_full = 1;
			} else {
				p_line_buffer++;
			}
		}
		
		// Line buffer is ready to be processed after it is full.
		// Implement a simple command parser
		if (line_buffer_full == 1) {
			udi_cdc_putc('\n');
		
			if (strcmp("hz", line_buffer) == 0) {
				char hz_string[64];
				sprintf(hz_string, "SYSCLK freq: %ld Hz\n\r", sysclk_get_cpu_hz());
				udi_cdc_write_buf(hz_string, strlen(hz_string));
			}
			else if (strcmp("led1 on", line_buffer) == 0) {
				udi_cdc_write_buf("LED1 turned on\n\r", strlen("LED1 turned on\n\r"));
				gpio_set_pin_low(LED1_GPIO);
			} else if (strcmp("led1 off", line_buffer) == 0) {
				udi_cdc_write_buf("LED1 turned off\n\r", strlen("LED1 turned off\n\r"));
				gpio_set_pin_high(LED1_GPIO);
			}
			else if (strcmp("led2 on", line_buffer) == 0) {
				udi_cdc_write_buf("LED2 turned on\n\r", strlen("LED2 turned on\n\r"));
				gpio_set_pin_low(LED2_GPIO);
			} else if (strcmp("led2 off", line_buffer) == 0) {
				udi_cdc_write_buf("LED2 turned off\n\r", strlen("LED2 turned off\n\r"));
				gpio_set_pin_high(LED2_GPIO);
			}
			else if (strcmp("relay1 on", line_buffer) == 0) {
				udi_cdc_write_buf("Relay_1 turned on\n\r", strlen("Relay_1 turned on\n\r"));
				gpio_set_pin_low(RELAY1_GPIO);
			} else if (strcmp("relay1 off", line_buffer) == 0) {
				udi_cdc_write_buf("Relay_1 turned off\n\r", strlen("Relay_1 turned off\n\r"));
				gpio_set_pin_high(RELAY1_GPIO);
			}
			else if (strcmp("relay2 on", line_buffer) == 0) {
				udi_cdc_write_buf("Relay_2 turned on\n\r", strlen("Relay_2 turned on\n\r"));
				gpio_set_pin_low(RELAY2_GPIO);
			} else if (strcmp("relay2 off", line_buffer) == 0) {
				udi_cdc_write_buf("Relay_2 turned off\n\r", strlen("Relay_2 turned off\n\r"));
				gpio_set_pin_high(RELAY2_GPIO);
			}
			else if (strcmp("adc", line_buffer) == 0) {
				char adc_string[32];
				uint8_t i_samples;
				
				measurement_data = 0;
				tc_enable(&TCC0);
 				while (!measurement_data_ready);
				
				for (i_samples = 0; i_samples < 128; i_samples++) {
					sprintf(adc_string, "%d\n\r", measurement_samples[i_samples]);
					udi_cdc_write_buf(adc_string, strlen(adc_string));
				}
				
 				sprintf(adc_string, "MS= %ld\n\r", measurement_data);
 				measurement_data_ready = 0;
 				udi_cdc_write_buf(adc_string, strlen(adc_string));				

 				sprintf(adc_string, "sample_count= %d\n\r", sample_count);
 				udi_cdc_write_buf(adc_string, strlen(adc_string));
			}
			else if (strcmp("owi init", line_buffer) == 0) {
				OWI_Init(OWI_BUS_1);
				udi_cdc_write_buf("OK\n\r", strlen("OK\n\r"));
			}
			else if (strcmp("owi presence", line_buffer) == 0) {
				if (OWI_DetectPresence(OWI_BUS_1) == OWI_BUS_1) {
					udi_cdc_write_buf("Detected\n\r", strlen("Detected\n\r"));
				} else {
					udi_cdc_write_buf("Not detected\n\r", strlen("Not detected\n\r"));
				}
			}
			else if (strcmp("owi write1", line_buffer) == 0) {
				OWI_WriteBit1(OWI_BUS_1);
				udi_cdc_write_buf("OK\n\r", strlen("OK\n\r"));
			}			
			else if (strcmp("owi write0", line_buffer) == 0) {
				OWI_WriteBit0(OWI_BUS_1);
				udi_cdc_write_buf("OK\n\r", strlen("OK\n\r"));
			}			
			else if (strcmp("owi read", line_buffer) == 0) {
				if (OWI_ReadBit(OWI_BUS_1) == OWI_BUS_1) {
					udi_cdc_write_buf("High\n\r", strlen("High\n\r"));
				} else {
					udi_cdc_write_buf("Low\n\r", strlen("Low\n\r"));
				}
			}
			else if (strcmp("owi search", line_buffer) == 0) {
				static OWI_device devices[MAX_DEVICES];
				char owi_string[64];
				uint8_t i;
				
				while(SearchBuses(devices, MAX_DEVICES, BUSES) != SEARCH_SUCCESSFUL);
				udi_cdc_write_buf("Device found:\n\r", strlen("Device found:\n\r"));
				
				for (i = 0; i < MAX_DEVICES; i++) {
					sprintf(owi_string, "%02X %02X %02X %02X %02X %02X %02X %02X\n\r",
						devices[i].id[0], devices[i].id[1], devices[i].id[2], devices[i].id[3],
						devices[i].id[4], devices[i].id[5], devices[i].id[6], devices[i].id[7]);
 					udi_cdc_write_buf(owi_string, strlen(owi_string));
				}
			}
			else if (strcmp("ds1820", line_buffer) == 0) {
				static OWI_device devices[MAX_DEVICES];
				OWI_device *ds1820;

				OWI_Init(OWI_BUS_1);
				
				while(SearchBuses(devices, MAX_DEVICES, BUSES) != SEARCH_SUCCESSFUL);
				udi_cdc_write_buf("Device found\n\r", strlen("Device found\n\r"));

			    ds1820 = FindFamily(DS1820_FAMILY_ID, devices, MAX_DEVICES);
				if (ds1820 == NULL) {
					udi_cdc_write_buf("No DS1820 found\n\r", strlen("No DS1820 found\n\r"));
				} else {
					char ds1820_string[64];
					int16_t temperature;
					temperature = DS1820_ReadTemperature((*ds1820).bus, (*ds1820).id);
				
 					sprintf(ds1820_string, "Temp: %f degC\n\r", temperature / 16.0);
 					udi_cdc_write_buf(ds1820_string, strlen(ds1820_string));
				}					
			}
			
			
			line_buffer_full = 0;
		}		
		
// 		gpio_toggle_pin(LED1_GPIO);
// 		gpio_toggle_pin(LED2_GPIO);
// 		_delay_ms(1000);
	}
}
